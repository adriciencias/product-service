package com.dws.productservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dws.productservice.dto.DtoProduct;
import com.dws.productservice.repository.RepositoryProduct;

@Service
public class ServiceProductImp implements ServiceProduct {

	@Autowired
	RepositoryProduct repositoryProduct;
	
	@Override
	public List<DtoProduct> getProducts() throws Exception {
		return repositoryProduct.getProducts();
	}

	@Override
	public DtoProduct getProduct(String code) throws Exception {
		try {
			return repositoryProduct.getProduct(code);
		}catch (Exception e) {
			throw new Exception("El producto no existe");
		}
	}

	@Override
	public void createProduct(DtoProduct Product) throws Exception {
		repositoryProduct.createProduct(Product);
	}

	@Override
	public void deleteProduct(String codigo) throws Exception {
		repositoryProduct.deleteProduct(codigo);
	}

	@Override
	public void updateStockProduct(DtoProduct product, String codigo) throws Exception {
		repositoryProduct.updateStockProduct(product, codigo);
	}

}
